@echo off
@net use Z: /delete /Y
 timeout 2
@net use Z: %1 /persistent:yes
 timeout 2
:: set folder path
set dump_path="Z:\Output\Backup\LoanPackage"

:: set min age of files and folders to delete
set max_days=%2

:: remove files from %dump_path%
forfiles -p %dump_path% -m *.* -d -%max_days% -c "cmd  /c  del /q @path""

:: remove sub directories from %dump_path%
forfiles -p %dump_path% -d -%max_days% -c "cmd /c IF @isdir == TRUE rd /S /Q @path"
pause
@net use Z: /delete /Y